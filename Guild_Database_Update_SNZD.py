import requests
from bs4 import BeautifulSoup
from pprint import pprint
import httplib2
from openpyxl.utils import get_column_letter
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import os

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Talibri Member Scraper v1.0'

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def fetch_memberids():
    """Shows basic usage of the Sheets API.

    Creates a Sheets API service object
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    #UPDATE244
    spreadsheetId = ''
    rangeName = 'Member_Declarations!F3:F60'
    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheetId, range=rangeName).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        return values

def update_guildDB(injection, rangeName):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    ## UPDATE244
    spreadsheetId = ''
    #rangeName = 'Member_Declarations!F3:F102'
    Body = {
        'values': injection
    }

    result = service.spreadsheets().values().update(
        spreadsheetId=spreadsheetId, range=rangeName, valueInputOption='RAW', body=Body).execute()
    #values = result.get('values', [])

    return

    if not values:
        print('No data found.')
    else:
        return values

def main():
    firstRun = True
    totalIteration = 0
    skillIteration = 0
    masteryIteration = 0
    memberIteration = 0
    firstRunInjection = []
    injection = []

    #### Fetch member profile id's from the TTF Master Google Docs Spreadsheet.
    #### This returns just a list of numbers that we can iterate over and pass into URLs.
    members = fetch_memberids()


    #### Log into Talibri using the below credentials.
    #### We need to grab the cookies from the response and pass them to all subsequent requests to the server.
    #### UPDATE244
    email = ''
    password = ''
    r = requests.get('https://talibri.com/users/sign_in')
    soup = BeautifulSoup(r.text, 'html.parser')
    utf = soup.find("input", attrs={'name': 'utf8'})['value']
    token = soup.find("input", attrs={'name': 'authenticity_token'})['value']
    cookies = r.cookies
    payload = {
        'user[email]': email,
        'user[password]': password,
        'user[remember_me]': '0',
        'commit': 'Log in',
        'utf8': utf,
        'authenticity_token': token
    }
    r = requests.post('https://talibri.com/users/sign_in', data=payload, cookies=cookies)
    cookies = r.cookies

    #### Start iterating over members
    #### idx (index) will be used for finding the correct column on the google docs sheet
    for idx, m in enumerate(members):
        totalIteration = 0
        injection = []

        ## Go to user's profile
        url = "https://talibri.com/user/{}/profile/".format(str(m[0]))
        r = requests.get(url, cookies=cookies)
        soup = BeautifulSoup(r.text, 'html.parser')
        csrf = soup.find("meta", {"name":"csrf-token"})['content']
        etag = r.headers["ETag"]
        cookies2 = r.cookies
        rightNav = soup.find("div", class_='alert alert-success text-center')
        ch1 = rightNav.contents[1]
        name = ch1.contents[0]

        print("Updating " + name)

        ## 'Click' the skills tab and parse out each skill
        skillsUrl = "https://talibri.com/user/{}/skills/".format(str(m[0]))
        skillsR = requests.get(skillsUrl, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies2)
        skillsParse = skillsR.text.split(".html(")[1].split(");")[0].replace("\\","")
        extrasoup = BeautifulSoup(skillsParse, 'html.parser')
        skills = extrasoup.find_all("div", class_='panel-heading')

        ## Now that we have a list of all skills, iterate over them and grab the masteries
        for skill in skills:
            if(len(skill.contents) > 4):
                totalIteration += 1
                skillIteration += 1
                skillName = skill.contents[1].contents[0]
                level = skill.contents[2].split(": ")[1].split("n")[0]
                experiencePercent = skill.contents[3].contents[1].contents[1].contents[0]
                masteryId = int(skill.contents[1]['href'].split("masteries/")[1])

                ## If this is the first iteration through, re-inject the skill and mastery names to make it sustainable in case of feature release
                if(firstRun):
                    firstRunInjection.append([skillName,""])

                #print("Skill: " + skillName)

                ## Start building our injection to google sheets.
                ## Beginning with the skill's level. Each item is it's own list object with how Google Sheets does their data parsing
                ## [A1, B1, C1, D1...], [A2, B2, C2, D2...]
                ## Since we are iterating vertically instead of horizontally, we only want one item for each row declaration
                injection.append([level.split(" ")[1]])

                ## 'Click' on the skill to get a list of Masteries for the current skill`
                masteryUrl = "https://talibri.com/users/{}/masteries/{}".format(str(m[0]), str(masteryId))
                masteryR = requests.get(masteryUrl, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies2)
                #masteryParse = masteryR.text.split(".html(")[1].split(");")[0].replace("\\","")
                hotsoup = BeautifulSoup(masteryR.text, 'html.parser')
                masteries = hotsoup.find_all("div", class_='panel-heading')
                for mastery in masteries:
                    totalIteration += 1
                    masteryIteration += 1
                    if(len(mastery.contents) > 2):
                        #masteryList.append(mastery.contents[0].lstrip())
                        ## nameAndLevel (nAL) will be a list of 2 items, the name of the mastery and it's corresponding level
                        #print(mastery)
                        nAL = mastery.contents[0].lstrip().split(" Level ")
                        #print("len(nAL): " + str(len(nAL)))
                        #print(nAL[1])
                        if(len(nAL) <= 1):
                            continue

                        injection.append([nAL[1].rstrip()])
                        if(firstRun):
                            firstRunInjection.append(["",nAL[0]])
            else:
                continue

        ## Now send injection list object to the sheet to update
        ## But first, we need to convert our memberIteration into a column letter
        column = get_column_letter(memberIteration + 3)
        sheetRange = "Guild_Database!{}3:{}{}".format(column,column,totalIteration + 3)

        ## The first run through of the script should update skill and mastery names
        ## This means no one has to manually maintain anything on this sheet! :O!!!!
        if(firstRun):
            update_guildDB(firstRunInjection, "Guild_Database!A3:B{}".format(totalIteration + 3))
            firstRun = False

        update_guildDB(injection, sheetRange)

        ## Increment the member number to adjust the column
        memberIteration += 1

if __name__ == "__main__":
    main()
