### Talibri GitLab Group

This group is provided as a ceneralized location for Talibri Scripts to be 
developed and run from. Each reposotory should be setup for one script including
its dependant files. There should also be a readme in each reposotory discribing
what the script does, and any dependancies.

#### User Access

Users requiring access to make any merges/issues can request access on the 
Talibri discord, and will be provided with Developer Access. On a case by case
basis, some users may receive Master Access.

#### Wiki

The wiki is provided as a resource for both developers and users looking to
enhance their gameplay with scripts. Talibri includes a link to the scripting
wiki page. The scripting page should be updated with all current script versions
of the scripts, including insturctions on how to install them.


If you need any assistance contact @kaine.adams (ign:Pirion)